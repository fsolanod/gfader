# Welcome to gfadeR

Please note that everything was created to evaluate measurements that were recorded using the *Analyst* software from *Sciex*.

## Requirements

You will need R for running gfadeR. This script has been tested with:
- R v4.0.3,
- RStudio v1.3.959, and 
- RTools v4.0
	
The mzML files were converted from Wiff files using
	ProteoWizard v3.0.19276-7124c5404, which can be downloaded from  [here](https://proteowizard.sourceforge.io/download.html)

fadeR requires the following R libraries:
- stringr, 
- mzR,
- MSnbase,
- DescTools,
- httr,
- htmlTable,
- emayili,
- dplyr,
- ggplot2,
- ggpubr,
- gridExtra,
- MassSpecWavelet,
- optparse,
- shiny,
- DT,
- lubridate,
- scales,
- writexl,
- tidyr,
- plotly,
- data.table,
- shiny,
- DT,
- shinydashboard,
- shinyWidgets,
- shinyFiles

If you have problems installing *mzR*, please follow these [instructions](https://bioconductor.org/packages/release/bioc/html/mzR.html)

If you have problems installing *MSnbase*, please follow these [instructions](https://bioconductor.org/packages/release/bioc/html/MSnbase.html)



## Customization

gfadeR script will read input data from the folders: *temp_data_wiff* and *temp_data* by default. Make sure these folders are created and place  your wiff data files from Analyst in "temp_data_wiff". 

gfadeR cannot read .wiff data files, therefore, .wiff data files need to be changed into mzML files by using the software msconvert of ProteoWizard. The software can be used as follows: Open the "Renviron" and change the folder location for the software msconvert of ProteoWizard. Please be aware to use / instead of \.

gfadeR can be operated by running the "server" script, which is located in the folder "lcms_shiny".

gfadeR has two modes to include your target analytes:

- Option 1: Write all substance information in the shiny application, according to the cocaine (COC) example. Substance information can be saved in .csv data files.
- Option 2: Import substance information from .csv data files, according to the carbamazepine (CBZ) example, which is located in the "example" folder.

Data processing can be started by executing "Start processing".

Your results are shown in "Results". You can dowload your results in various file formats and check the integration of your substances
